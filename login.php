<?php
session_start();
if (isset($_POST['password']) && isset($_POST['user']) && isset($_POST['database'])) {
    $_SESSION['dbPassword'] = $_POST['password'];
    $_SESSION['dbUser'] = $_POST['user'];
    $_SESSION['database'] = $_POST['database'];
    try {
        $connection = new PDO("mysql:host=localhost;dbname=".$_SESSION['database'], $_SESSION['dbUser'], $_SESSION['dbPassword']);
        $_SESSION['authentication'] = true;
        header('Location:index.php');
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
        header('Location:index.html');
    }
}